﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace dec.topsis.proyecto
{
    public partial class formMain : Form
    {
        private int cantCriterios { get; set; }
        private int cantAlternativas { get; set; }
        private List<String> nombresCriterios { get; set; }
        private List<TextBox> txtboxCriterios { get; set; }
        private List<TextBox> txtboxPesos { get; set; }
        private List<float> listaValoresPesos { get; set; }
        private List<Label> listaLabelsCriterios { get; set; }

        public formMain()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            gbCriterios.Visible = true;
            gbCriterios.Enabled = true;
            
            limpiarGrilla();
            nombresCriterios = new List<string>();
            
            
            
            validarAlternativasCriterios();
            generarNombresCriterios();
           
        }

        public void copiarMatriz()
        {
            for (int i = 0; i <= cantCriterios; i++)
            {
                for (int j = 0; j <= cantAlternativas; j++)
                {
                    dgvNormalizada.Rows[j].Cells[i].Value = dgvMatriz.Rows[j].Cells[i].Value;
                    dgvNormalizadaYPonderada.Rows[j].Cells[i].Value = dgvMatriz.Rows[j].Cells[i].Value;
                }
            }
        }

        private void limpiarGrilla()
        {
            eliminarTxtBoxCriterios();
            txtboxCriterios = null;
            cantAlternativas = 0;
            cantCriterios = 0;
            dgvMatriz.DataSource = null;
            dgvMatriz.Rows.Clear();
            dgvMatriz.ColumnCount = 0;

            dgvNormalizada.DataSource = null;
            dgvNormalizada.Rows.Clear();
            dgvNormalizada.ColumnCount = 0;

        }

        private void generarMatriz()
        {
            foreach (String nombre in nombresCriterios)
            {
                //matriz
                DataGridViewColumn column = new DataGridViewColumn();
                column.CellTemplate = new DataGridViewTextBoxCell();
                column.Name = nombre;
                column.Width = 100;
                column.ReadOnly = false;
                dgvMatriz.Columns.Add(column);

                //matriz normalizada
                DataGridViewColumn column2 = new DataGridViewColumn();
                column2.CellTemplate = new DataGridViewTextBoxCell();
                column2.Name = nombre;
                column2.Width = 100;
                column2.ReadOnly = false;
                dgvNormalizada.Columns.Add(column2);

                //matriz normalizada y ponderada
                DataGridViewColumn column3 = new DataGridViewColumn();
                column3.CellTemplate = new DataGridViewTextBoxCell();
                column3.Name = nombre;
                column3.Width = 100;
                column3.ReadOnly = false;
                dgvNormalizadaYPonderada.Columns.Add(column3);

            }
            for (int i = 0; i <= cantAlternativas; i++)
            {
                int index = dgvMatriz.Rows.Add();
                int index2 = dgvNormalizada.Rows.Add();
                int index3 = dgvNormalizadaYPonderada.Rows.Add();
                DataGridViewRow row = new DataGridViewRow();
                dgvMatriz.Rows[index].Cells[0].Value = "Alternativa_" + (i + 1);
                dgvNormalizada.Rows[index].Cells[0].Value = "Alternativa_" + (i + 1);
                dgvNormalizadaYPonderada.Rows[index].Cells[0].Value = "Alternativa_" + (i + 1);

                if (i == cantAlternativas)
                {
                    dgvMatriz.Rows[index].Cells[0].Value = "";
                    dgvNormalizada.Rows[index2].Cells[0].Value = "";
                    dgvNormalizadaYPonderada.Rows[index2].Cells[0].Value = "";
                }
            }
            
            dgvMatriz.AutoSize = true;
            dgvNormalizada.AutoSize = true;
            dgvNormalizadaYPonderada.AutoSize = true;
            dgvMatriz.Refresh();
            dgvNormalizada.Refresh();
            dgvNormalizadaYPonderada.Refresh();
        }

        private void eliminarTxtBoxCriterios()
        {
            try
            {
                for (int i = 0; i <= cantCriterios; i++)
                {
                    TextBox txt = txtboxCriterios.Find(item => item.Name == ("txtCriterio" + i));
                    Label lbl = listaLabelsCriterios.Find(item => item.Name == "lblCriterio" + i);
                    panel1.Controls.Remove(txt);
                    panel1.Controls.Remove(lbl);
                }

            }
            catch { }
            
        }

        private void generarNombresCriterios()
        {
            listaLabelsCriterios = new List<Label>();
            txtboxCriterios = new List<TextBox>();
            txtboxPesos = new List<TextBox>();
            for (int i = 0; i < cantCriterios; i++)
            {
                TextBox txtCriterio = new TextBox();
                Label labelCriterio = new Label();
                TextBox txtPesoCriterio = new TextBox();

                labelCriterio.Text = "Criterio y peso " + (i + 1) + ":";
                labelCriterio.Name = "lblCriterio"+(i + 1);
                txtCriterio.Width = 90;
                labelCriterio.Width = 90;
                txtPesoCriterio.Width = 30;

                //Altura de los campos
                txtCriterio.Height = 20;
                txtPesoCriterio.Height = 20;
                labelCriterio.Height = 20;

                //Pocision de los campos
                labelCriterio.Top = (i * 25) + 10;
                txtCriterio.Top = (i * 25) + 7;
                txtPesoCriterio.Top = (i * 25) + 7;

                txtCriterio.Left = 100;
                labelCriterio.Left = 5;
                txtPesoCriterio.Left = 200;

                txtCriterio.Show();
                txtPesoCriterio.Show();
                txtCriterio.Visible = true;
                txtPesoCriterio.Visible = true;
                
                //Agregarlos al control
                panel1.Controls.Add(labelCriterio);
                panel1.Controls.Add(txtCriterio);
                panel1.Controls.Add(txtPesoCriterio);

                //asignar nombre a los textbox que se crean
                txtCriterio.Name = "txtCriterio" + (i + 1);
                txtboxCriterios.Add(txtCriterio);
                listaLabelsCriterios.Add(labelCriterio);

                txtPesoCriterio.Name = "txtPesoCriterio" + (i + 1);
                txtboxPesos.Add(txtPesoCriterio);
                
                panel1.Height += txtCriterio.Height+15;
                btnAgregarNombresCriterios.Top += txtCriterio.Height + 25;
                
            }
            panel1.Height = panel1.Height;
            btnAgregarNombresCriterios.Top = btnAgregarNombresCriterios.Top / 2;
        }


        private void validarAlternativasCriterios()
        {
            try
            {
                cantCriterios = Convert.ToInt32(txtCantCriterios.Text);
                cantAlternativas = Convert.ToInt32(txtCantAlternativas.Text);

                if (String.IsNullOrEmpty(txtCantAlternativas.Text) || String.IsNullOrEmpty(txtCantCriterios.Text))
                {
                    throw new Exception("Debe completar todos los campos.");
                }
                if (Convert.ToInt32(txtCantAlternativas.Text) < 0)
                {
                    throw new Exception("El parametro Cantidad de alternativas tiene que ser un numero entero positivo");
                }
                if (Convert.ToInt32(txtCantCriterios.Text) <= 0)
                {
                    throw new Exception("El parametro Cantidad de criterios tiene que ser un numero entero positivo");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Atención",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Exclamation,
                                            MessageBoxDefaultButton.Button1);
            }

        }

        public Boolean validarPesos()
        {
            float pesoTotal = 0f;
            float peso = 0f;
            listaValoresPesos = new List<float>();
            listaValoresPesos.Clear();

            foreach (TextBox txt in txtboxPesos)
            {
                try
                {
                    peso = float.Parse(txt.Text);
                    listaValoresPesos.Add(peso);
                    pesoTotal += peso;

                
                    if (String.IsNullOrEmpty(txt.Text) || String.IsNullOrEmpty(txt.Text))
                    {
                        throw new Exception("Debe completar todos los campos.");
                    }
                    if (float.Parse(txt.Text) <= 0)
                    {
                        throw new Exception("Los pesos de los Criterios deben tener un valor entre 0 y 1.");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Atención",
                                                MessageBoxButtons.OK,
                                                MessageBoxIcon.Exclamation,
                                                MessageBoxDefaultButton.Button1);
                    
                    break;
                }

            }
            if (pesoTotal != 1)
            {
                MessageBox.Show("La suma total de los pesos de todos los " +
                    "criterios debe ser igual a 1. Revise los valores " +
                    "ingresados.", "Atención",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                return false;
            }
            else
            {
                return true;
            }

        }

        private void btnAgregarNombresCriterios_Click(object sender, EventArgs e)
        {
            menuCaclularTopsis.Enabled = true;
            gbTopsis.Visible = true;

            dgvMatriz.Enabled = true;
            dgvMatriz.Visible = true;
            
            if (nombresCriterios.Count >= 1)
            {
                dgvMatriz.Columns.Clear();
                nombresCriterios.Clear();
                dgvNormalizada.Columns.Clear();
            }
            nombresCriterios.Add(""); //PARA LA PRIMERA COLUMNA DE LAS ALTERNATIVAS
            foreach (TextBox txt in txtboxCriterios)
            {
                nombresCriterios.Add(txt.Text);
            }

            //si los pesos son los correctos se puede generar la matriz
            if ( validarPesos()==true)
            {
                generarMatriz();
            }

        }

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void btnTopsis_Click(object sender, EventArgs e)
        {
            /*
            dgvNormalizada.Visible = true;
            generarMatrixDeDecision();
            copiarMatriz();
            generarMatrizNormailizada();
            dgvMatriz.Visible = true;
            generarMatrixNormalizadaYPonderada();
            dgvNormalizadaYPonderada.Visible = true;
            Point point = new Point(800, 610);
            gbTopsis.AutoScrollOffset = point;*/
        }

        private void generarMatrixNormalizadaYPonderada()
        {
            double norm = 0d;
            int i = 1;

            foreach (float peso in listaValoresPesos)
            {
                for (int j = 0; j < cantAlternativas; j++)
                {
                        norm = Double.Parse(dgvNormalizadaYPonderada.Rows[j].Cells[i].Value.ToString()) /
                            Double.Parse(dgvNormalizadaYPonderada.Rows[cantAlternativas].Cells[i].Value.ToString())*peso;

                        dgvNormalizadaYPonderada.Rows[j].Cells[i].Value = norm;
                }
                norm = 0d;
                i++;
            }
            Point point = new Point(22, dgvNormalizada.Location.Y + dgvNormalizada.Height + 5);
            dgvNormalizadaYPonderada.ReadOnly = true;
            dgvNormalizadaYPonderada.Rows.Remove(dgvNormalizadaYPonderada.Rows[cantAlternativas]);
            dgvNormalizadaYPonderada.Location = point;
            
        }

        private void generarMatrizNormailizada()
        {
            double norm = 0d;

            for (int i = 1; i <= cantCriterios; i++)
            {
                for (int j = 0; j < cantAlternativas; j++)
                {
                    norm = Double.Parse(dgvNormalizada.Rows[j].Cells[i].Value.ToString() )/
                        Double.Parse(dgvNormalizada.Rows[cantAlternativas].Cells[i].Value.ToString());
                    dgvNormalizada.Rows[j].Cells[i].Value = norm;
                    
                }
                norm = 0d;
                
                dgvNormalizada.ReadOnly = true;
            }
            Point point = new Point(22, dgvMatriz.Location.Y + dgvMatriz.Height + 5);
            dgvNormalizada.Rows.Remove(dgvNormalizada.Rows[cantAlternativas]);
            dgvNormalizada.Location = point;
        }

        private void generarMatrixDeDecision()
        {
            double suma=0d, suma2=0d;
            
            try
            {
                for (int i = 1; i <= cantCriterios; i++)
                {
                    for (int j = 0; j < cantAlternativas; j++)
                    {
                        suma += Double.Parse(dgvMatriz.Rows[j].Cells[i].Value.ToString());
                        dgvMatriz.Rows[cantAlternativas].Cells[i].Value = suma;
                        //dgvNormalizada.Rows[cantAlternativas].Cells[i].Value = suma2;
                    }
                    suma = 0d;
                    suma2 = 0d;
                }
            }
            catch {
                MessageBox.Show("Ingrese un valor valido en la grilla.");
            }
        }

        private void calcularTopsisToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgvMatriz.Rows[0].Cells[1].Selected = true;
            dgvNormalizada.Visible = true;
            generarMatrixDeDecision();
            copiarMatriz();
            generarMatrizNormailizada();
            dgvMatriz.Visible = true;
            generarMatrixNormalizadaYPonderada();
            dgvNormalizadaYPonderada.Visible = true;
            Point point = new Point(800, 610);
            gbTopsis.AutoScrollOffset = point;
        }

        private void menuNuevo_Click(object sender, EventArgs e)
        {
            menuCaclularTopsis.Enabled = true;
            eliminarTxtBoxCriterios();

            gbCriterios.Enabled = true;
            gbCantCriterios.Enabled = true;
            gbCriterios.Visible = false;
            dgvMatriz.Enabled = true;
            dgvMatriz.Visible = true;

            dgvNormalizada.Enabled = true;
            dgvNormalizada.Visible = true;

            nombresCriterios.Clear();
            listaValoresPesos.Clear();
        }


    }
}
