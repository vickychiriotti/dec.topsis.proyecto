﻿namespace dec.topsis.proyecto
{
    partial class formMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gbCantCriterios = new System.Windows.Forms.GroupBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.txtCantCriterios = new System.Windows.Forms.TextBox();
            this.lblCantCriterios = new System.Windows.Forms.Label();
            this.txtCantAlternativas = new System.Windows.Forms.TextBox();
            this.lblCantAlternativas = new System.Windows.Forms.Label();
            this.dgvMatriz = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAgregarNombresCriterios = new System.Windows.Forms.Button();
            this.gbCriterios = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nuevoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuNuevo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAbrir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCaclularTopsis = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExportar = new System.Windows.Forms.ToolStripMenuItem();
            this.dgvNormalizada = new System.Windows.Forms.DataGridView();
            this.dgvNormalizadaYPonderada = new System.Windows.Forms.DataGridView();
            this.gbTopsis = new System.Windows.Forms.GroupBox();
            this.generarPDFToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generarCSVToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbCantCriterios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMatriz)).BeginInit();
            this.gbCriterios.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNormalizada)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNormalizadaYPonderada)).BeginInit();
            this.gbTopsis.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbCantCriterios
            // 
            this.gbCantCriterios.Controls.Add(this.btnAceptar);
            this.gbCantCriterios.Controls.Add(this.txtCantCriterios);
            this.gbCantCriterios.Controls.Add(this.lblCantCriterios);
            this.gbCantCriterios.Controls.Add(this.txtCantAlternativas);
            this.gbCantCriterios.Controls.Add(this.lblCantAlternativas);
            this.gbCantCriterios.Location = new System.Drawing.Point(12, 28);
            this.gbCantCriterios.Name = "gbCantCriterios";
            this.gbCantCriterios.Size = new System.Drawing.Size(269, 111);
            this.gbCantCriterios.TabIndex = 1;
            this.gbCantCriterios.TabStop = false;
            this.gbCantCriterios.Text = "Alternativas y criterios";
            // 
            // btnAceptar
            // 
            this.btnAceptar.BackColor = System.Drawing.Color.Silver;
            this.btnAceptar.Location = new System.Drawing.Point(49, 82);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(157, 23);
            this.btnAceptar.TabIndex = 4;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = false;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // txtCantCriterios
            // 
            this.txtCantCriterios.Location = new System.Drawing.Point(177, 50);
            this.txtCantCriterios.Name = "txtCantCriterios";
            this.txtCantCriterios.Size = new System.Drawing.Size(29, 20);
            this.txtCantCriterios.TabIndex = 3;
            this.txtCantCriterios.Text = "2";
            this.txtCantCriterios.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCantCriterios
            // 
            this.lblCantCriterios.AutoSize = true;
            this.lblCantCriterios.Location = new System.Drawing.Point(64, 53);
            this.lblCantCriterios.Name = "lblCantCriterios";
            this.lblCantCriterios.Size = new System.Drawing.Size(107, 13);
            this.lblCantCriterios.TabIndex = 2;
            this.lblCantCriterios.Text = "Cantidad de Criterios:";
            // 
            // txtCantAlternativas
            // 
            this.txtCantAlternativas.Location = new System.Drawing.Point(177, 23);
            this.txtCantAlternativas.Name = "txtCantAlternativas";
            this.txtCantAlternativas.Size = new System.Drawing.Size(29, 20);
            this.txtCantAlternativas.TabIndex = 1;
            this.txtCantAlternativas.Text = "2";
            this.txtCantAlternativas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblCantAlternativas
            // 
            this.lblCantAlternativas.AutoSize = true;
            this.lblCantAlternativas.Location = new System.Drawing.Point(46, 26);
            this.lblCantAlternativas.Name = "lblCantAlternativas";
            this.lblCantAlternativas.Size = new System.Drawing.Size(125, 13);
            this.lblCantAlternativas.TabIndex = 0;
            this.lblCantAlternativas.Text = "Cantidad de Alternativas:";
            // 
            // dgvMatriz
            // 
            this.dgvMatriz.AccessibleDescription = "";
            this.dgvMatriz.AccessibleName = "";
            this.dgvMatriz.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dgvMatriz.AllowUserToAddRows = false;
            this.dgvMatriz.AllowUserToDeleteRows = false;
            this.dgvMatriz.AllowUserToResizeRows = false;
            this.dgvMatriz.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvMatriz.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvMatriz.BackgroundColor = System.Drawing.Color.White;
            this.dgvMatriz.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvMatriz.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMatriz.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvMatriz.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMatriz.GridColor = System.Drawing.Color.Silver;
            this.dgvMatriz.Location = new System.Drawing.Point(22, 26);
            this.dgvMatriz.MultiSelect = false;
            this.dgvMatriz.Name = "dgvMatriz";
            this.dgvMatriz.RowHeadersVisible = false;
            this.dgvMatriz.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvMatriz.Size = new System.Drawing.Size(15, 17);
            this.dgvMatriz.StandardTab = true;
            this.dgvMatriz.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.AutoScrollMargin = new System.Drawing.Size(0, 500);
            this.panel1.Location = new System.Drawing.Point(6, 19);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(257, 58);
            this.panel1.TabIndex = 3;
            // 
            // btnAgregarNombresCriterios
            // 
            this.btnAgregarNombresCriterios.BackColor = System.Drawing.Color.Silver;
            this.btnAgregarNombresCriterios.Location = new System.Drawing.Point(49, 83);
            this.btnAgregarNombresCriterios.Name = "btnAgregarNombresCriterios";
            this.btnAgregarNombresCriterios.Size = new System.Drawing.Size(157, 23);
            this.btnAgregarNombresCriterios.TabIndex = 0;
            this.btnAgregarNombresCriterios.Text = "Agregar";
            this.btnAgregarNombresCriterios.UseVisualStyleBackColor = false;
            this.btnAgregarNombresCriterios.Click += new System.EventHandler(this.btnAgregarNombresCriterios_Click);
            // 
            // gbCriterios
            // 
            this.gbCriterios.Controls.Add(this.btnAgregarNombresCriterios);
            this.gbCriterios.Controls.Add(this.panel1);
            this.gbCriterios.Location = new System.Drawing.Point(12, 145);
            this.gbCriterios.Name = "gbCriterios";
            this.gbCriterios.Size = new System.Drawing.Size(269, 495);
            this.gbCriterios.TabIndex = 4;
            this.gbCriterios.TabStop = false;
            this.gbCriterios.Text = "Criterios";
            this.gbCriterios.Visible = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Silver;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoToolStripMenuItem,
            this.menuExportar,
            this.menuCaclularTopsis});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1192, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // nuevoToolStripMenuItem
            // 
            this.nuevoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuNuevo,
            this.menuAbrir});
            this.nuevoToolStripMenuItem.Name = "nuevoToolStripMenuItem";
            this.nuevoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.nuevoToolStripMenuItem.Text = "Archivo";
            this.nuevoToolStripMenuItem.Click += new System.EventHandler(this.nuevoToolStripMenuItem_Click);
            // 
            // menuNuevo
            // 
            this.menuNuevo.Name = "menuNuevo";
            this.menuNuevo.Size = new System.Drawing.Size(109, 22);
            this.menuNuevo.Text = "Nuevo";
            this.menuNuevo.Click += new System.EventHandler(this.menuNuevo_Click);
            // 
            // menuAbrir
            // 
            this.menuAbrir.Name = "menuAbrir";
            this.menuAbrir.Size = new System.Drawing.Size(109, 22);
            this.menuAbrir.Text = "Abrir";
            // 
            // menuCaclularTopsis
            // 
            this.menuCaclularTopsis.Enabled = false;
            this.menuCaclularTopsis.Name = "menuCaclularTopsis";
            this.menuCaclularTopsis.Size = new System.Drawing.Size(97, 20);
            this.menuCaclularTopsis.Text = "Calcular Topsis";
            this.menuCaclularTopsis.Click += new System.EventHandler(this.calcularTopsisToolStripMenuItem_Click);
            // 
            // menuExportar
            // 
            this.menuExportar.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generarPDFToolStripMenuItem,
            this.generarCSVToolStripMenuItem});
            this.menuExportar.Name = "menuExportar";
            this.menuExportar.Size = new System.Drawing.Size(63, 20);
            this.menuExportar.Text = "Exportar";
            // 
            // dgvNormalizada
            // 
            this.dgvNormalizada.AllowUserToAddRows = false;
            this.dgvNormalizada.AllowUserToDeleteRows = false;
            this.dgvNormalizada.AllowUserToResizeRows = false;
            this.dgvNormalizada.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvNormalizada.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvNormalizada.BackgroundColor = System.Drawing.Color.White;
            this.dgvNormalizada.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvNormalizada.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNormalizada.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvNormalizada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNormalizada.GridColor = System.Drawing.Color.Silver;
            this.dgvNormalizada.Location = new System.Drawing.Point(6, 49);
            this.dgvNormalizada.MultiSelect = false;
            this.dgvNormalizada.Name = "dgvNormalizada";
            this.dgvNormalizada.RowHeadersVisible = false;
            this.dgvNormalizada.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvNormalizada.Size = new System.Drawing.Size(15, 21);
            this.dgvNormalizada.StandardTab = true;
            this.dgvNormalizada.TabIndex = 8;
            this.dgvNormalizada.Visible = false;
            // 
            // dgvNormalizadaYPonderada
            // 
            this.dgvNormalizadaYPonderada.AllowUserToAddRows = false;
            this.dgvNormalizadaYPonderada.AllowUserToDeleteRows = false;
            this.dgvNormalizadaYPonderada.AllowUserToResizeRows = false;
            this.dgvNormalizadaYPonderada.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvNormalizadaYPonderada.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvNormalizadaYPonderada.BackgroundColor = System.Drawing.Color.White;
            this.dgvNormalizadaYPonderada.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvNormalizadaYPonderada.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvNormalizadaYPonderada.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvNormalizadaYPonderada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNormalizadaYPonderada.GridColor = System.Drawing.Color.Silver;
            this.dgvNormalizadaYPonderada.Location = new System.Drawing.Point(6, 82);
            this.dgvNormalizadaYPonderada.MultiSelect = false;
            this.dgvNormalizadaYPonderada.Name = "dgvNormalizadaYPonderada";
            this.dgvNormalizadaYPonderada.RowHeadersVisible = false;
            this.dgvNormalizadaYPonderada.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvNormalizadaYPonderada.Size = new System.Drawing.Size(17, 19);
            this.dgvNormalizadaYPonderada.StandardTab = true;
            this.dgvNormalizadaYPonderada.TabIndex = 9;
            this.dgvNormalizadaYPonderada.Visible = false;
            // 
            // gbTopsis
            // 
            this.gbTopsis.AutoSize = true;
            this.gbTopsis.Controls.Add(this.dgvMatriz);
            this.gbTopsis.Controls.Add(this.dgvNormalizada);
            this.gbTopsis.Controls.Add(this.dgvNormalizadaYPonderada);
            this.gbTopsis.Location = new System.Drawing.Point(289, 28);
            this.gbTopsis.Name = "gbTopsis";
            this.gbTopsis.Size = new System.Drawing.Size(800, 610);
            this.gbTopsis.TabIndex = 10;
            this.gbTopsis.TabStop = false;
            this.gbTopsis.Text = "Complete los valores correspondientes segun la Alternativa y criterio y luego hag" +
    "a clic en Calcular topsis:";
            this.gbTopsis.Visible = false;
            // 
            // generarPDFToolStripMenuItem
            // 
            this.generarPDFToolStripMenuItem.Name = "generarPDFToolStripMenuItem";
            this.generarPDFToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.generarPDFToolStripMenuItem.Text = "Generar PDF";
            // 
            // generarCSVToolStripMenuItem
            // 
            this.generarCSVToolStripMenuItem.Name = "generarCSVToolStripMenuItem";
            this.generarCSVToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.generarCSVToolStripMenuItem.Text = "Generar CSV";
            // 
            // formMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1192, 745);
            this.Controls.Add(this.gbTopsis);
            this.Controls.Add(this.gbCriterios);
            this.Controls.Add(this.gbCantCriterios);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "formMain";
            this.Text = "Topsis";
            this.TransparencyKey = System.Drawing.Color.Peru;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.gbCantCriterios.ResumeLayout(false);
            this.gbCantCriterios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMatriz)).EndInit();
            this.gbCriterios.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNormalizada)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNormalizadaYPonderada)).EndInit();
            this.gbTopsis.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbCantCriterios;
        private System.Windows.Forms.TextBox txtCantCriterios;
        private System.Windows.Forms.Label lblCantCriterios;
        private System.Windows.Forms.TextBox txtCantAlternativas;
        private System.Windows.Forms.Label lblCantAlternativas;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.DataGridView dgvMatriz;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAgregarNombresCriterios;
        private System.Windows.Forms.GroupBox gbCriterios;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nuevoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuExportar;
        private System.Windows.Forms.DataGridView dgvNormalizada;
        private System.Windows.Forms.DataGridView dgvNormalizadaYPonderada;
        private System.Windows.Forms.GroupBox gbTopsis;
        private System.Windows.Forms.ToolStripMenuItem menuCaclularTopsis;
        private System.Windows.Forms.ToolStripMenuItem menuNuevo;
        private System.Windows.Forms.ToolStripMenuItem menuAbrir;
        private System.Windows.Forms.ToolStripMenuItem generarPDFToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generarCSVToolStripMenuItem;
    }
}

