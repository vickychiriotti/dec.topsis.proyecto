﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dec.topsis.proyecto
{
    public class Criterio
    {
        private int IdCriterio { get; set; }
        private String Nombre { get; set; }
        private float Peso { get; set; }
        private Boolean EsMinimizante { get; set; }

        public Criterio(int idCriterio, String nombre, float peso, Boolean esMinimizante)
        {
            this.IdCriterio = idCriterio;
            this.Nombre = nombre;
            this.Peso = peso;
            this.EsMinimizante = esMinimizante;
        }
    }
}
